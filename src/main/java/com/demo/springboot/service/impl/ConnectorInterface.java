package com.demo.springboot.service.impl;

import com.demo.springboot.domain.dto.*;

import java.util.List;

public interface ConnectorInterface {

    void connect();

    int createID(String nick);

    int getRange(int id);

    List getDrinks();

    List getIngredients();

    ListUsersDto getUsers();

    void setRange(int id);

    ListTasteDto getTastes();

    int addDrink(NewDrinkDto newDrinkDto);

    int addIngredient(String name, int percent, String description);

    ListDrinksDto getFavoriteDrinks(int id);

    int addFavoriteDrink(int user_id,int drink_id);

    int addOwnedIngredient(OwnedIngredientDto ownedIngredientDto);

    ListIngredientsDto getOwnedIngredient(int id);

    int deleteFavoriteDrinks(int user_id, int drink_id);

    int deleteFavoriteIngredient(int user_id, int ingredient_id);

    ListDrinksDto getDrinksWithOwnedIngredient(int user_id);

}

