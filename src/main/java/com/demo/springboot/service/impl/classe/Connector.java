package com.demo.springboot.service.impl.classe;

import com.demo.springboot.domain.dto.*;
import com.demo.springboot.service.impl.ConnectorInterface;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Connector implements ConnectorInterface {

    private String passDB = "lol123123";
    private String loginDB = "postgres";
    private String urlDB = "jdbc:postgresql://localhost:5432/postgres";
    private String driver = "org.postgresql.Driver";
    private Connection con;
    private PreparedStatement stmt;
    private PreparedStatement stmtSecond;
    private PreparedStatement stmtThird;

    public Connector() {
        connect();
    }

    @Override
    public void connect() {
        try {
            Class.forName(this.driver);
            this.con = DriverManager.getConnection(this.urlDB, this.loginDB, this.passDB);

        } catch (Exception var4) {
            System.out.println(var4.getMessage());
        }
    }

    public int createID(String nick){
        try{
            stmt = this.con.prepareStatement("SELECT * FROM public.Users Order BY user_id ");
            ResultSet rs = stmt.executeQuery();
            int id=1;

            while(rs.next()){

                if(nick.equals(rs.getString(3))){
                    return 0;
                }

                if(id == rs.getInt(1)){
                    id++;
                }


            }

            stmt = con.prepareStatement( String.format("INSERT INTO Users VALUES (%2d,0,'%s')",id,nick));
            stmt.execute();

            return id;

        }catch (Exception var4) {
            System.out.println(var4.getMessage());
            return 0;
        }
    }

    @Override
    public int getRange(int id) {
        try{
            stmt = con.prepareStatement(String.format("SELECT * FROM public.Users WHERE user_id=%2d",id));
            ResultSet rs = stmt.executeQuery();
            int range = 9;

            while(rs.next()){
                range = rs.getInt(2);
            }

            return range;

        }catch (Exception var4) {
            System.out.println(var4.getMessage());
            return 997;
        }
    }

    @Override
    public List getDrinks() {
        // Należy dodać sprawdzanie wielkości liter, gdyż wszystko zalezy jak dane zostały wpisane w bazie
        List<DrinkDto> drinks = new ArrayList<>();


        try{

            int id;
            String taste,method = "error";
            stmt = this.con.prepareStatement("SELECT * FROM public.drink");
            ResultSet rs = stmt.executeQuery();


            DrinkDto drinkDto;

            while(rs.next()){
                taste = "";
                id = rs.getInt(1);

                stmtSecond = this.con.prepareStatement(String.format("SELECT drink.drink_id ,taste,method FROM taste,drink_taste,drink WHERE drink.drink_id = drink_taste.drink_id AND drink_taste.taste_id = taste.taste_id AND drink.drink_id = %2d ",id));
                ResultSet rs2 = stmtSecond.executeQuery();

                while (rs2.next()){
                    if(id == rs2.getInt(1)){
                        taste = rs2.getString(2);
                        method = rs2.getString(3);
                    }
                }

                drinkDto = new DrinkDto(id,changeWord(rs.getString(2)),taste,method);
                drinks.add(drinkDto);

            }


            return new ArrayList<>(drinks);

        }catch (Exception var4) {
            System.out.println(var4.getMessage());
            return new ArrayList();
        }


    }

    @Override
    public List getIngredients() {

        List<IngredientDto> ingredients = new ArrayList<>();


        try{
            IngredientDto ingredientDto;



            stmt = this.con.prepareStatement("SELECT * FROM ingredient");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){
                ingredientDto = new IngredientDto(rs.getInt(1),changeWord(rs.getString(2)),rs.getInt(3),rs.getString(4));

                ingredients.add(ingredientDto);
            }

            return new ArrayList<>(ingredients);
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return new ArrayList();
        }

    }

    @Override
    public ListUsersDto getUsers() {

        List<UserDto> users = new ArrayList<>();
        List<UserDto> premiumUsers = new ArrayList<>();

        UserDto userDto;


        try{

            stmt = this.con.prepareStatement("select * from users");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){
                userDto = new UserDto(rs.getInt(1),rs.getString(3),rs.getInt(2));
                if(rs.getInt(2) == 0){
                    users.add(userDto);
                }else if (rs.getInt(2) == 1){
                    premiumUsers.add(userDto);
                }

            }


            return new ListUsersDto(users,premiumUsers);
        }catch (Exception var4){
            System.out.print(var4.getMessage());
            return new ListUsersDto(users,premiumUsers);
        }


    }

    @Override
    public void setRange(int id) {
        int range = 2;
        try{
            this.stmt = con.prepareStatement(String.format("SELECT range FROM users WHERE user_id =%2d", id));
            ResultSet rs = this.stmt.executeQuery();

            while (rs.next()){
                range = rs.getInt(1);
            }

            if(range == 0){
                this.stmt = con.prepareStatement(String.format("UPDATE users SET range=1 WHERE user_id=%2d",  id));
                this.stmt.execute();
            }else if(range == 1){
                this.stmt = con.prepareStatement(String.format("UPDATE users SET range=0 WHERE user_id=%2d", id));
                this.stmt.execute();
            }else{
                System.out.println("ACCESS DENIED");
            }

        }catch (Exception var4){
            System.out.println(var4.getMessage());
        }
    }

    @Override
    public ListTasteDto getTastes() {
        List<TasteDto> tastes= new ArrayList<>();

        try {
            this.stmt = con.prepareStatement("SELECT * FROM taste");
            ResultSet rs = this.stmt.executeQuery();

            while (rs.next()){

                tastes.add(new TasteDto(rs.getInt(1),rs.getString(2)));
            }

            return new ListTasteDto(tastes);
        }catch (Exception var4){

            return new ListTasteDto(tastes);
        }


    }


    private String changeWord(String word) {

        StringBuilder newWord = new StringBuilder();

        for(int i=0;i<word.length();i++){
            if(i==0){
                newWord.append(String.valueOf(word.charAt(0)).toUpperCase());
            }else{
                newWord.append(String.valueOf(word.charAt(i)).toLowerCase());
            }
        }

        return newWord.toString();
    }
    public List<RecipeDto> getDrinkRecipe(int id){

        List<RecipeDto> recipe = new ArrayList<>();

        try{
            stmt = this.con.prepareStatement(String.format("SELECT ingredient.ingredient_name, recipe.proportion, ingredient.percent FROM drink,recipe,ingredient WHERE drink.drink_id = recipe.drink_id and recipe.ingredient_id = ingredient.ingredient_id and drink.drink_id=%2d",id));
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){


                RecipeDto recipeDto = new RecipeDto(rs.getString(1),rs.getString(2),rs.getInt(3));

                recipe.add(recipeDto);
            }



            return new ArrayList<>(recipe);
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return new ArrayList<>(recipe);
        }

    }

    @Override
    public int addDrink(NewDrinkDto newDrinkDto) {

        String drink_Name = newDrinkDto.getDrinkName();
        if(drink_Name.equals("")) return 0;

        String method = newDrinkDto.getMethod();

        int taste_id = newDrinkDto.getId_taste();

        List<NewDrinkRecipeDto> recipe = new ArrayList<NewDrinkRecipeDto>(newDrinkDto.getRecipe());
        int recipe_size = recipe.size();
        if(recipe_size == 0) return 0;
        if(method.length() == 0) return 0;

        try{


            //Set id for new drink
            this.stmt = this.con.prepareStatement("Select * FROM drink ORDER BY drink_id");
            ResultSet rs = this.stmt.executeQuery();
            int drink_id = 0;

            while (rs.next()){
                if(rs.getString(2).equals(drink_Name)){
                    return 0;
                }
                drink_id = rs.getInt(1);
            }
            drink_id++;

            //Check taste
            this.stmt = this.con.prepareStatement(String.format("Select * FROM taste WHERE taste_id=%2d",taste_id));
            rs = this.stmt.executeQuery();
            int number= 0;

            while (rs.next()) number++;

            if(number==0) return 0;

            number = 0;


            // Checking the ingredient use
            int i;
            for(i=0; i<recipe_size; i++) {
                this.stmt = this.con.prepareStatement("SELECT * FROM ingredient WHERE ingredient_id =" + recipe.get(i).getId());
                rs = this.stmt.executeQuery();

                while (rs.next()) number++;
            }
            if(number!=recipe_size) return 0;

            // Add ingredients for table recipe in DataBase
            for (i=0; i<recipe_size; i++){
                this.stmt = this.con.prepareStatement(String.format("INSERT INTO recipe VALUES (%2d, %2d, '%s')",drink_id, recipe.get(i).getId(),recipe.get(i).getProportion()));
                this.stmt.execute();
            }

            // Add drink
            this.stmt = this.con.prepareStatement(String.format("INSERT INTO drink VALUES (%2d, '%s', '%s')",drink_id,drink_Name,method));
            this.stmt.execute();

            // Add data taste for drink_taste table in Database
            this.stmt = this.con.prepareStatement(String.format("INSERT INTO drink_taste VALUES (%2d, %2d)",drink_id,taste_id));
            this.stmt.execute();

            this.stmt = this.con.prepareStatement(String.format("INSERT INTO drink_statistic VALUES (%2d, 0,0)",drink_id));
            this.stmt.execute();



            return 1;
        }catch (Exception var4){
            System.out.println(var4.toString());
            return 0;
        }

    }

    @Override
    public int addIngredient(String name, int percent, String description) {
        if(percent < 0 || percent > 100) return 0;

        name = name.toLowerCase();


        try{
            int id=1;

            this.stmt = this.con.prepareStatement("SELECT * FROM ingredient ORDER BY ingredient_name DESC");
            ResultSet rs = this.stmt.executeQuery();

            while (rs.next()){
                if(rs.getString(2).equals(name)) return 0;
                id++;
            }

            this.stmt = this.con.prepareStatement(String.format("INSERT INTO ingredient VALUES(%2d,'%s',%2d,'%s')",id,name,percent,description));
            this.stmt.execute();

            this.stmt = this.con.prepareStatement(String.format("INSERT INTO liquor_statistic VALUES(%2d,0,0,'now()')",id));
            this.stmt.execute();

            return 1;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return 0;
        }

    }

    @Override
    public ListDrinksDto getFavoriteDrinks(int id) {
        List<DrinkDto> drinks = new ArrayList<>();

        try {
            this.stmt = this.con.prepareStatement(String.format("SELECT d.drink_id,d.drink_name,t.taste, d.method FROM favorite_drink as fd,drink as d, taste as t, drink_taste as ds WHERE fd.user_id=%2d AND fd.drink_id=d.drink_id AND d.drink_id=ds.drink_id AND ds.taste_id=t.taste_id ORDER BY d.drink_id DESC",id));
            ResultSet rs = this.stmt.executeQuery();

            while (rs.next()){

                drinks.add(new DrinkDto(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4)));

            }

            return new ListDrinksDto(drinks);
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return new ListDrinksDto(drinks);
        }
    }

    @Override
    public int addFavoriteDrink(int user_id, int drink_id) {

        if(user_id <=0 || drink_id <=0 ) return 0;

        ResultSet rs;
        int i=0;
        try{

            this.stmt = this.con.prepareStatement(String.format("SELECT * FROM users WHERE user_id=%2d",user_id));
            rs = this.stmt.executeQuery();

            while (rs.next()) i++;
            if(i==0) return 0;

            i=0;
            this.stmt = this.con.prepareStatement(String.format("SELECT * FROM drink WHERE drink_id=%2d",drink_id));
            rs = this.stmt.executeQuery();

            while (rs.next()) i++;
            if (i==0) return 0;

            i=0;
            this.stmt = this.con.prepareStatement(String.format("SELECT * FROM favorite_drink WHERE drink_id=%2d AND user_id=%2d",drink_id,user_id));
            rs = this.stmt.executeQuery();

            while (rs.next()) i++;
            if(i==1) return 0;


            this.stmt = this.con.prepareStatement(String.format("INSERT INTO favorite_drink VALUES (%2d,%2d)",user_id,drink_id));
            this.stmt.execute();


            return 1;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return 0;
        }

    }

    @Override
    public int addOwnedIngredient(OwnedIngredientDto ownedIngredientDto) {

        int user_id = ownedIngredientDto.getUserid();

        if(user_id <=0 ) return 0;



        List<IdDto> IdList = new ArrayList<IdDto>(ownedIngredientDto.getIngredients());

        for (IdDto idDto : IdList) {
            if (idDto.getId() <= 0) return 0;
        }

        ResultSet rs;
        int i=0;
        try{

            this.stmt = this.con.prepareStatement(String.format("SELECT * FROM users WHERE user_id=%2d",user_id));
            rs = this.stmt.executeQuery();

            while (rs.next()) i++;
            if(i==0) return 0;


            for(IdDto idDto: IdList){

                i=0;
                this.stmt = this.con.prepareStatement(String.format("SELECT * FROM ingredient WHERE ingredient_id=%2d",idDto.getId()));
                rs = this.stmt.executeQuery();

                while (rs.next()) i++;
                if (i==0) return 0;
            }

            this.stmt = this.con.prepareStatement(String.format("DELETE FROM owned_alcohol WHERE user_id=%2d",user_id));
            this.stmt.execute();

            for(IdDto idDto: IdList){

                i=0;
                this.stmt = this.con.prepareStatement(String.format("INSERT INTO owned_alcohol VALUES (%2d,%2d)",user_id,idDto.getId()));
                this.stmt.execute();
            }

            return 1;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return 0;
        }

    }

    @Override
    public ListIngredientsDto getOwnedIngredient(int id) {

        if(id <= 0) return new ListIngredientsDto();

        try {
            this.stmt = this.con.prepareStatement(String.format("SELECT i.ingredient_id, ingredient_name, i.percent, i.description  FROM ingredient as i, owned_alcohol as oa, users as u WHERE i.ingredient_id=oa.ingredient_id AND oa.user_id = u.user_id AND u.user_id=%2d",id));
            ResultSet rs = this.stmt.executeQuery();
            List<IngredientDto> ingredient = new ArrayList<IngredientDto>();

            while (rs.next()){
                ingredient.add(new IngredientDto(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getString(4)));
            }

            return new ListIngredientsDto(ingredient);

        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return new ListIngredientsDto();
        }

    }

    @Override
    public int deleteFavoriteDrinks(int user_id, int drink_id) {
        if(user_id <=0 || drink_id <=0) return 0;
        int i=0;

        try {
            this.stmt = this.con.prepareStatement(String.format("SELECT * from favorite_drink WHERE user_id=%2d AND drink_id=%2d",user_id,drink_id));
            ResultSet rs = this.stmt.executeQuery();

            while (rs.next()){
                i++;
            }
            if(i==0) return 0;

            this.stmt = this.con.prepareStatement(String.format("DELETE FROM favorite_drink WHERE user_id=%2d AND drink_id=%2d",user_id,drink_id));
            this.stmt.execute();

            return 1;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return 0;
        }

    }

    @Override
    public int deleteFavoriteIngredient(int user_id, int ingredient_id) {
        if(user_id <=0 || ingredient_id <=0) return 0;
        int i=0;

        try {
            this.stmt = this.con.prepareStatement(String.format("SELECT * from owned_alcohol  WHERE user_id=%2d AND ingredient_id=%2d",user_id,ingredient_id));
            ResultSet rs = this.stmt.executeQuery();

            while (rs.next()){
                i++;
            }
            if(i==0) return 0;

            this.stmt = this.con.prepareStatement(String.format("DELETE FROM owned_alcohol WHERE user_id=%2d AND ingredient_id=%2d",user_id,ingredient_id));
            this.stmt.execute();

            return 1;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return 0;
        }
    }

    @Override
    public ListDrinksDto getDrinksWithOwnedIngredient(int user_id) {
        List drinks = new ArrayList();
        ResultSet rs;
        try{

            ListIngredientsDto ownedListIngredients = getOwnedIngredient(user_id);

            List<IngredientDto> ownedIngredient = new ArrayList<IngredientDto>(ownedListIngredients.getIngredients());

            List<Integer> idList = new ArrayList();

            for(int i=0; i<ownedIngredient.size();i++){
                int id = ownedIngredient.get(i).getIngredient_id();
                this.stmt = this.con.prepareStatement(String.format("SELECT * FROM recipe WHERE ingredient_id=%2d",id));
                rs = this.stmt.executeQuery();

                while (rs.next()){
                    idList.add(rs.getInt(1));
                }
            }

            idList = idList.stream().distinct().collect(Collectors.toList());

            List<DrinkDto> allDrink = new ArrayList(getDrinks());

            for(int i=0; i<idList.size(); i++){
                for(int j=0; j<allDrink.size() ;j++){
                    if(idList.get(i) == allDrink.get(j).getId_drink()){
                        drinks.add(allDrink.get(j));
                    }
                }
            }

            return new ListDrinksDto(drinks);
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return new ListDrinksDto(drinks);
        }

    }


}


