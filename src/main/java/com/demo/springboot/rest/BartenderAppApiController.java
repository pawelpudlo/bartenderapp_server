package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.*;
import com.demo.springboot.service.impl.classe.Connector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.sound.sampled.Line;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class BartenderAppApiController {

    private Connector con;

    public BartenderAppApiController(Connector con) {
        this.con = con;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(BartenderAppApiController.class);


    @RequestMapping(value = "/setId/{nick}", method = RequestMethod.GET)
    public ResponseEntity<ReturnIdDto> createId(@PathVariable("nick") String nick) {

        Connector con = new Connector();
        int id = con.createID(nick);

        final ReturnIdDto returnIdDto = new ReturnIdDto(id);

        LOGGER.info("Create now ID: " + id);

        return new ResponseEntity<>(returnIdDto,HttpStatus.OK);
    }

    @RequestMapping(value = "/getRange/{id}", method = RequestMethod.GET)
    public ResponseEntity<RangeDto> getRange(@PathVariable("id") int id) {

        Connector con = new Connector();
        int range = con.getRange(id);

        final RangeDto rangeDto = new RangeDto(range);

        LOGGER.info("Range for user with id: "+ id + " is: " + range);

        return new ResponseEntity<>(rangeDto,HttpStatus.OK);
    }

    @RequestMapping(value = "/getDrinks",method = RequestMethod.GET)
    public ResponseEntity<ListDrinksDto> getDrinks(){


        Connector con = new Connector();
        List drinksList = con.getDrinks();
        final ListDrinksDto listDrinksDto = new ListDrinksDto(drinksList);

        LOGGER.info("List Drinks: " + con.getDrinks());


        return new ResponseEntity<>(listDrinksDto,HttpStatus.OK);
    }

    @RequestMapping(value = "/getDrinkRecipe/{id}",method = RequestMethod.GET)
    public ResponseEntity<ListRecipeDto> getDrinkRecipe(@PathVariable("id") int id){

        Connector con = new Connector();
        List<RecipeDto> recipeList = con.getDrinkRecipe(id);
        final ListRecipeDto listRecipeDto = new ListRecipeDto(recipeList);

        LOGGER.info("Recipe List: " + recipeList.toString());

        return new ResponseEntity<>(listRecipeDto,HttpStatus.OK);
    }

    @RequestMapping(value = "/getIngredients",method = RequestMethod.GET)
    public ResponseEntity<ListIngredientsDto> getIngredient(){

        Connector con = new Connector();
        List ingredientList = con.getIngredients();
        final ListIngredientsDto listIngredientsDto = new ListIngredientsDto(ingredientList);

        return new ResponseEntity<>(listIngredientsDto,HttpStatus.OK);
    }

    @RequestMapping(value = "/getUsers",method = RequestMethod.GET)
    public ResponseEntity<ListUsersDto> getUsers(){

        return new ResponseEntity<>(con.getUsers(),HttpStatus.OK);
    }

    @RequestMapping(value = "/setRange/{id}",method = RequestMethod.GET)
    public ResponseEntity setRange(@PathVariable("id") int id){
        con.setRange(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/getTastes", method = RequestMethod.GET)
    public ResponseEntity<ListTasteDto> getTastes(){

        return new ResponseEntity<>(con.getTastes(),HttpStatus.OK);
    }

    @PostMapping(value = "/addDrink")
    public ResponseEntity<InfoDto> addDrink(@RequestBody NewDrinkDto newDrinkDto) {
        int i = this.con.addDrink(newDrinkDto);

        if(i==1){
            InfoDto infoDto = new InfoDto("IS GOOD");

            return new ResponseEntity<>(infoDto,HttpStatus.OK);
        }else{
            InfoDto infoDto = new InfoDto("Error");

            return new ResponseEntity<>(infoDto,HttpStatus.BAD_REQUEST);
        }
    }
    // localhost:8080/api/addIngredient?name="x"&percent=40&description="x"
    @PostMapping(value = "/addIngredient")
    public ResponseEntity<InfoDto> addIngredient(@RequestParam(value = "name",required = false) String name,
                                               @RequestParam(value = "percent",required = false) int percent,
                                               @RequestParam(value = "description",required = false) String description){

        int i=con.addIngredient(name,percent,description);

        if(i==1){
            InfoDto infoDto = new InfoDto("YOU ADD NEW INGREDIENT");

            return new ResponseEntity<>(infoDto,HttpStatus.OK);
        }else{
            InfoDto infoDto = new InfoDto("Error");

            return new ResponseEntity<>(infoDto,HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(value = "/getFavoriteDrink/{id}")
    public ResponseEntity<ListDrinksDto> getFavoriteDrinks(@PathVariable(value = "id") int id){
        return new ResponseEntity<>(con.getFavoriteDrinks(id),HttpStatus.OK);
    }

    // localhost:8080/api/addFavoriteDrink?user_id=6&drink_id=6
    @PostMapping(value = "/addFavoriteDrink")
    public ResponseEntity<Void> addFavoriteDrink(@RequestParam(value = "user_id",required = false) int user_id,
                                                 @RequestParam(value = "drink_id",required = false) int drink_id){

        int check = this.con.addFavoriteDrink(user_id,drink_id);
        if(check==1){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    //localhost:8080/api/addOwnedIngredient?user_id=11&ingredient_id=2
    @PostMapping(value = "/addOwnedIngredient")
    public ResponseEntity<Void> addOwnedIngredient(@RequestBody OwnedIngredientDto ownedIngredientDto){

        int check = this.con.addOwnedIngredient(ownedIngredientDto);
        if(check==1){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    //localhost:8080/api/deleteOwnedIngredient?user_id=2&ingredient_id=2
    @DeleteMapping(value = "/deleteOwnedIngredient")
    public ResponseEntity<Void> deleteOwnedIngredient(@RequestParam(value = "user_id",required = false) int user_id,
                                                         @RequestParam(value = "ingredient_id",required = false) int ingredient_id){

        int i= this.con.deleteFavoriteIngredient(user_id,ingredient_id);

        if(i==1){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }


    }

    //localhost:8080/api/deleteFavoriteDrink?user_id=2&drink_id=1
    @DeleteMapping(value = "/deleteFavoriteDrink")
    public ResponseEntity<Void> deleteFavoriteDrinks(@RequestParam(value = "user_id",required = false) int user_id,
                                                      @RequestParam(value = "drink_id",required = false) int drink_id){
        int i= this.con.deleteFavoriteDrinks(user_id,drink_id);

        if(i==1){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    //localhost:8080/api/getOwnedIngredient/2
    @GetMapping(value = "/getOwnedIngredient/{id}")
    public ResponseEntity<ListIngredientsDto> getOwnedIgredient(@PathVariable(value = "id") int id){
        return new ResponseEntity<>(this.con.getOwnedIngredient(id),HttpStatus.OK);
    }



    //localhost:8080/api/getDrinksWithOwnedIngredient/2
    @GetMapping(value = "/getDrinksWithOwnedIngredient/{id}")
    public ResponseEntity<ListDrinksDto> getDrinksWithOwnedIngredient(@PathVariable(value = "id") int id){

        return new ResponseEntity<>(con.getDrinksWithOwnedIngredient(id),HttpStatus.OK);
    }

}
