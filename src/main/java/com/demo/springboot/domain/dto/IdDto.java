package com.demo.springboot.domain.dto;

public class IdDto {

    private int id;

    public IdDto(int id) {
        this.id = id;
    }

    public IdDto() {
    }

    public int getId() {
        return id;
    }
}
