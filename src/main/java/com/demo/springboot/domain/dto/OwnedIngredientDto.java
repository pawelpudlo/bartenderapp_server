package com.demo.springboot.domain.dto;

import java.util.List;

public class OwnedIngredientDto {

    private int userid;
    private List<IdDto> ingredients;

    public OwnedIngredientDto(int userid, List<IdDto> ingredients) {
        this.userid = userid;
        this.ingredients = ingredients;
    }

    public OwnedIngredientDto() {
    }

    public int getUserid(){
        return userid;
    }

    public List getIngredients() {
        return ingredients;
    }
}
