package com.demo.springboot.domain.dto;

import java.util.List;

public class ListUsersDto {

    private List users;
    private List users_premium;

    public ListUsersDto(List users, List users_premium) {
        this.users = users;
        this.users_premium = users_premium;
    }

    public List getUsers() {
        return users;
    }

    public List getUsers_premium() {
        return users_premium;
    }
}
