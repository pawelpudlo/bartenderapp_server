package com.demo.springboot.domain.dto;

public class ReturnIdDto {

    private int id=0;


    public ReturnIdDto(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
