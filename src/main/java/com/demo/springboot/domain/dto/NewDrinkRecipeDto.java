package com.demo.springboot.domain.dto;

public class NewDrinkRecipeDto {

    private int id;
    private String proportion;

    public NewDrinkRecipeDto(int id, String proportion) {
        this.id = id;
        this.proportion = proportion;
    }

    public NewDrinkRecipeDto() {
    }

    public int getId() {
        return id;
    }

    public String getProportion() {
        return proportion;
    }
}
