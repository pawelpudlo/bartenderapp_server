package com.demo.springboot.domain.dto;

public class RangeDto {

    private int range=3;

    public RangeDto(int range) {
        this.range = range;
    }

    public int getRange() {
        return range;
    }
}
