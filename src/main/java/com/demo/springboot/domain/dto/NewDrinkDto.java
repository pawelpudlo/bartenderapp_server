package com.demo.springboot.domain.dto;

import java.util.List;

public class NewDrinkDto {

    private String drinkName;
    private int id_taste;
    private List<NewDrinkRecipeDto> recipe;
    private String method;

    public NewDrinkDto(String drinkName, int id_taste, List<NewDrinkRecipeDto> recipe, String method) {
        this.drinkName = drinkName;
        this.id_taste = id_taste;
        this.recipe = recipe;
        this.method = method;
    }

    public NewDrinkDto() {
    }

    public String getDrinkName() {
        return drinkName;
    }

    public int getId_taste() {
        return id_taste;
    }

    public List getRecipe() {
        return recipe;
    }

    public String getMethod() {
        return method;
    }
}
