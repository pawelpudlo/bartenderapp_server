package com.demo.springboot.domain.dto;

public class TasteDto {

    private int id;
    private String name;

    public TasteDto(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
