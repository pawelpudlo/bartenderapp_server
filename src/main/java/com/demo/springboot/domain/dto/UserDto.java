package com.demo.springboot.domain.dto;

public class UserDto {

    private int user_id;
    private String nick;
    private int range;


    public UserDto(int user_id, String nick, int range) {
        this.user_id = user_id;
        this.nick = nick;
        this.range = range;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getNick() {
        return nick;
    }

    public int getRange() {
        return range;
    }
}
